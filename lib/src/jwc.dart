
import 'package:json_annotation/json_annotation.dart';
import 'package:crypto_keys/crypto_keys.dart';
import 'package:jose/jose.dart';
import "package:pointycastle/digests/sha256.dart";
import 'package:uuid/uuid.dart';

import './serialisers.dart';
import './verify_result.dart';
import './content.dart';
import './subject.dart';
import './identifier_wrapper.dart';
import './util.dart';

import 'dart:async';
import 'dart:typed_data';

part 'jwc.g.dart';

/// An interface to test if a certificate is trusted
abstract class RootTester {
    bool isTrustedRoot(JsonWebCertificate jwc);
}

/// Represents a JSON Web Certificate
@JsonSerializable(checked: true)
class JsonWebCertificate {

    JsonWebCertificate({ required this.signature, this.parent });
    JsonWebCertificate._({ required this.signature, this.parent, required ProtectedContent content }):
      _content = content;

    /// The Maximum length of a trust chain
    static final MAX_DEPTH = 4;

    /// The JSON Web Signature protecting the [ProtectedContent]
    @JsonKey(required: true)
    @JwsSerialiser()
    final JsonWebSignature signature;

    /// The Certificate used to sign this certificate
    ///
    /// This **must** be [null] for self signed certificates.
    /// This can be null for other certificates, but this makes verification more difficult.
    @JsonKey(
      required: false,
      toJson: _pToJson
    )
    final JsonWebCertificate? parent;

    // Should not be nessesarc, but the package json_serializable does things.....
    static dynamic _pToJson(JsonWebCertificate? c) => c == null ? null : c.toJson();

    ProtectedContent? _content = null;

    /// Get the [ProtectedContent] from [signature]
    ///
    /// Uses a caches object if present
    ProtectedContent get content {
        _content ??= ProtectedContent.fromJson(
            signature.unverifiedPayload.jsonContent as Map<String, dynamic>
        );
        return _content!;
    }

    /// Checks if the certificate is self signed
    bool get selfSigned => content.key.keyId == signature.commonHeader.keyId;

    /// Computes the SHA256 hash of the signatures payload
    Uint8List thumbprintSha256() {
        final sha256 = SHA256Digest();
        return sha256.process(Uint8List.fromList(signature.unverifiedPayload.data));
    }

    /// Verifies if the certificate is valid.
    ///
    /// [roots] represents the trusted ca root certificates.
    Future<VerifyResult> verify(RootTester roots) async => await _verifyRecursive(roots, 0);

    FutureOr<VerifyResult> _verifyRecursive(RootTester roots, int steps) {
        if (steps > MAX_DEPTH) {
            return VerifyResult.invalid(reason: InvalidReason.ChainTooLong);
        }

        if (! content.ValidNow()) {
            return VerifyResult.invalid(reason: InvalidReason.CertExpired);
        }
        
        if (selfSigned) {
            return _verifySelfSigned(roots);
        } else if (parent != null) {
            return _verifyWithParent(roots, steps);
        } else {
            return VerifyResult.invalid(reason: InvalidReason.ParentUnknown);
        }
    }

    Future<VerifyResult> _verifySelfSigned(RootTester roots) => VerifyResult.allAsync([
        () => _parentAllowedToIssue(this),
        () => verifyWith(this),
        () => _verifyTrustedRoot(roots),
    ]);

    Future<VerifyResult> _verifyWithParent(RootTester roots, int steps) => VerifyResult.allAsync([
        () => _parentAllowedToIssue(parent!),
        () => verifyWith(parent!),
        () => parent!._verifyRecursive(roots, steps + 1)
    ]);

    VerifyResult _parentAllowedToIssue(JsonWebCertificate parent) => 
        parent.content.usageAllowed(Usage.VerifyCertificate) ? VerifyResult.valid() : VerifyResult.invalid(reason: InvalidReason.ParentCannotIssue);

    VerifyResult _verifyTrustedRoot(RootTester roots) =>
        roots.isTrustedRoot(this) ? VerifyResult.valid() : VerifyResult.invalid(reason: InvalidReason.UntrustedRoot);

    /// Verifies **only the signature** with teh given [parent].
    /// 
    /// The returned [VerifyResult] can only be invalid with the reason [InvalidReason.InvalidSignature].
    Future<VerifyResult> verifyWith(JsonWebCertificate parent) async {
        final keySet = JsonWebKeyStore()
            ..addKey(parent.content.key);

        try {
            await signature.getPayload(keySet);
            return VerifyResult.valid();
        } on JoseException catch (e) {
            return VerifyResult.invalid(reason: InvalidReason.InvalidSignature, exception: e);
        }
    }

    /// Creates and signes a new JWC
    factory JsonWebCertificate.create({ 
        JsonWebCertificate? parent, 
        String? authority,
        required JsonWebKey private,
        required JsonWebKey public,
        required Subject subject,
        Iterable<Usage> usage = const[],
    }) {

      verifyArgument(parent != null || authority != null);

      final a = parent?.content.commonName ?? authority;
        
        final content = ProtectedContent.create(
            authority: a!,
            key: public,
            subject: subject,
            usage: Set.unmodifiable(usage)
        );

        final builder = JsonWebSignatureBuilder()
            ..jsonContent = content.toJson()
            ..addRecipient(private);
        
        return JsonWebCertificate._(
            signature: builder.build(),
            parent: parent,
            content: content
        );
    }

    /// Generates a key pair and creates a JWC
    static CaRoot createWithoutKey({
        required JsonWebKey parentPrivate,
        required JsonWebCertificate parent,
        String? commonName,
        Subject? subject,
        Iterable<Usage> usage = const[]
    }) {

        verifyArgument(commonName != null || subject != null);

        subject ??= Subject(commonName: commonName!);

        final pair = KeyPair.generateEc(IdentifierWrapper.P512);
        final kid = Uuid().v4();
        final private = JsonWebKey.fromCryptoKeys(privateKey: pair.privateKey, keyId: kid);
        final public = JsonWebKey.fromCryptoKeys(publicKey: pair.publicKey, keyId: kid);

        final cert = JsonWebCertificate.create(
            parent: parent,
            private: parentPrivate,
            public: public,
            subject: subject,
            usage: usage
        );

        return CaRoot(cert, private);
    }

    /// Creates a self signed JWC
    factory JsonWebCertificate.selfSigned({
        required JsonWebKey private,
        required JsonWebKey public,
        required Subject subject,
        List<Usage> usage = const[],
        required String authority,
    }) {
        verifyArgument(private.keyId == public.keyId);
        return JsonWebCertificate.create(
            private: private,
            public: public,
            subject: subject,
            usage: usage,
            authority: authority
        );
    }

    /// Create a ca root certificate from the given key pair
    factory JsonWebCertificate.caRoot(JsonWebKey private, JsonWebKey public) {
        verifyArgument(private.keyId == public.keyId);
        final content = ProtectedContent.createCaRoot(public);
        final builder = JsonWebSignatureBuilder()
            ..jsonContent = content.toJson()
            ..addRecipient(private);
        
        return JsonWebCertificate._(
            signature: builder.build(),
            content: content
        );
    }

    /// Creates a new ca root and generates the key pair
    static CaRoot newCaRoot() {
        final pair = KeyPair.generateEc(IdentifierWrapper.P512);
        final kid = Uuid().v4();
        final private = JsonWebKey.fromCryptoKeys(privateKey: pair.privateKey, keyId: kid);
        final public = JsonWebKey.fromCryptoKeys(publicKey: pair.publicKey, keyId: kid);

        return CaRoot(
          JsonWebCertificate.caRoot(private, public),
          private
        );
    }

    // JSON
    factory JsonWebCertificate.fromJson(Map<String, dynamic> json) => _$JsonWebCertificateFromJson(json);
    Map<String, dynamic> toJson() => _$JsonWebCertificateToJson(this);
}

/// Represents a [JsonWebCertificate] with its private key
class CaRoot {
    const CaRoot(this.certificate, this.privateKey);
    
    final JsonWebKey privateKey;
    final JsonWebCertificate certificate;
}
