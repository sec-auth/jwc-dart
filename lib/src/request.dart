
import 'package:jose/jose.dart';
import 'package:crypto_keys/crypto_keys.dart';
import 'package:uuid/uuid.dart';

import './request_content.dart';
import './content.dart';
import './verify_result.dart';
import './jwc.dart';
import './identifier_wrapper.dart';
import './subject.dart';
import './util.dart';

import 'dart:async';

/// A Request to create a [JsonWebCertificate]
///
/// This is basically a Json Web Signature containing the content [RequestContent]
class JsonWebCertificateRequest {

    JsonWebCertificateRequest(this.signature, this.content);
    
    JsonWebCertificateRequest.signature(JsonWebSignature signature):
        this(signature, _parseContent(signature));

    /// The Signature containing the protected content
    final JsonWebSignature signature;

    /// The parsed content from [signature]
    final RequestContent content;

    bool get isRenewRequest => signature.recipients.length > 1;

    static RequestContent _parseContent(JsonWebSignature signature) {
        final json = signature.unverifiedPayload.jsonContent as Map<String, dynamic>;
        return RequestContent.fromJson(json);
    }

    /// Validates the signature of the JWC Request
    Future<VerifyResult> validate([JsonWebCertificate? oldCert]) async {
        final keys = JsonWebKeyStore()
            ..addKey(content.key);

        try {
            await signature.getPayload(keys);
        } on JoseException catch (e) {
            return VerifyResult.invalid(
                reason: InvalidReason.InvalidSignature,
                exception: e
            );
        }

        if(isRenewRequest && oldCert != null) {
          final oldKeys = JsonWebKeyStore()
            ..addKey(oldCert.content.key);
          try {
            await signature.getPayload(oldKeys);
          } on JoseException catch (e) {
            return VerifyResult.invalid(
                reason: InvalidReason.InvalidSignature,
                exception: e
            );
          }
        }

        return VerifyResult.valid();
    }

    /// Approves the request by signing it with [parent]
    ///
    /// [usage] or [usageOverrider] can be used to override the requested usage. 
    /// Either [usage] or [usageOverrider] or both must be null.
    ///
    /// [subjectOverrider] can be used to modify the requested subject.
    /// If your implementations supports the generation of the commonName, [nameGenerator] can be given.
    /// [nameGenerator] is called if no commonName is given.
    JsonWebCertificate approve(JsonWebCertificate parent, JsonWebKey parentPrivate, { 
            Set<Usage>? usage, 
            Overrider<Set<Usage>>? usageOverrider,
            BiOverrider<Map<String, String>, Subject>? subjectOverrider,
            CommonNameGenerator? nameGenerator
        }) {

        verifyArgument(usage == null || usageOverrider == null, 'Either usage or usageOverrider or both must be null');
        verifyArgument(subjectOverrider == null || nameGenerator == null, 'nameGenerator has no effect if subjectOverrider is given');

        subjectOverrider ??= (rs) => Subject.fromRequestSubject(rs, nameGenerator);

        usage ??= content.usage;
        if (usageOverrider != null) {
            usage = usageOverrider(usage);
        }

        return JsonWebCertificate.create(
            parent: parent,
            private: parentPrivate,
            public: content.key,
            subject: subjectOverrider(content.subject),
            usage: usage,
        );
    }

    factory JsonWebCertificateRequest.create(RequestContent content, JsonWebKey private, [JsonWebKey? oldPrivate]) {
        verifyArgument(private.keyId == content.key.keyId, 'The private Key must belong to the public key in the content');

        final builder = JsonWebSignatureBuilder()
            ..jsonContent = content.toJson()
            ..addRecipient(private);

        if(oldPrivate != null) {
          builder.addRecipient(oldPrivate);
        }

        return JsonWebCertificateRequest(builder.build(), content);
    }

    static GeneratedRequest createWithoutKey({ Map<String, String>? subject,  Set<Usage> usage = const {}, String? commonName, JsonWebKey? oldPrivate }) {
        final pair = KeyPair.generateEc(IdentifierWrapper.P512);
        final kid = Uuid().v4();
        final private = JsonWebKey.fromCryptoKeys(privateKey: pair.privateKey, keyId: kid);
        final public = JsonWebKey.fromCryptoKeys(publicKey: pair.publicKey, keyId: kid);

        subject ??= commonName != null ? { 'commonName': commonName } : {};

        final content = RequestContent(
            key: public,
            subject: subject,
            usage: usage
        );

        final request = JsonWebCertificateRequest.create(content, private, oldPrivate);

        return GeneratedRequest(request, private);
    }

    // JSON
    factory JsonWebCertificateRequest.fromJson(Map<String, dynamic> json) => JsonWebCertificateRequest.signature(
        JsonWebSignature.fromJson(json)
    );

    Map<String, dynamic> toJson() => signature.toJson();

}

/// A [JsonWebCertificateRequest] with its private key
class GeneratedRequest {
    
    const GeneratedRequest(this.request, this.private);
    
    final JsonWebCertificateRequest request;
    final JsonWebKey private;
}

/// Overrides an [input] of [T1] to an output of [T2].
typedef T2 BiOverrider<T1, T2>(T1 input);

/// Overrides an [input] of [T].
typedef T Overrider<T>(T input);
