import 'package:crypto_keys/crypto_keys.dart';

/// Identifier for EC Algorithms
/// 
/// The Package crypto_keys does not export a constructor for the identifier.
/// So this helper class is nessessary to create EC Key Pairs
class IdentifierWrapper implements Identifier {

  static const P512 = IdentifierWrapper('curve/P-521');

  const IdentifierWrapper(this.name);
  
  final String name;
}
