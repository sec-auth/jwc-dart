
import 'package:json_annotation/json_annotation.dart';
import 'package:uuid/uuid.dart';
import 'package:jose/jose.dart';

import './serialisers.dart';
import './subject.dart';
import './util.dart';

part 'content.g.dart';

final uuid = Uuid();

/// Represents the Content of a [JsonWebCertificate].
@JsonSerializable(checked: true)
class ProtectedContent {

    const ProtectedContent({
      required this.serialNumber,
      required this.authority,
      required this.notAfter,
      required this.notBefore,
      required this.key,
      required this.subject,
      required this.usage,
    });

    /// Generates [notBefore], [notAfter] and [serialNumber].
    ///
    /// The Certtificate is valid from now on for four years
    ProtectedContent.create({
      required this.authority,
      required this.key,
      required this.subject,
      required this.usage,
    }):
      serialNumber = uuid.v4(),
      notBefore = DateTime.now(),
      notAfter = DateTime.now().add(Duration(days: 365) * 4);

  /// Generates all fields exept for [key]
  ///
  /// The Generates content is for a certificate authority root with a randomly generates [commonName]
  factory ProtectedContent.createCaRoot(JsonWebKey key, [ String? id ]) {
  
    id ??= uuid.v4();

    return ProtectedContent.create(
      authority: id,
      key: key,
      subject: Subject(commonName: id),
      usage: Set.from([ Usage.VerifyCertificate ]),
    );
  }

    /// The Serial Number of the Certificate
    ///
    /// This **must** be generates by the certificate [authority] while signing the certificate. This **must** be unique within the [authority].
    @JsonKey(required: true)
    final String serialNumber;

    /// The unique identifier of the authority
    ///
    /// This can be a random string or a reverse domain name like de.google.ca
    @JsonKey(required: true)
    final String authority;

    /// The first time where the certificate is valid
    @JsonKey(required: true)
    final DateTime notBefore;

    /// The last time where the certificate is valid
    @JsonKey(required: true)
    final DateTime notAfter;

    /// The public key as a [JsonWebKey] 
    @JwkSerialiser()
    @JsonKey(required: true)
    final JsonWebKey key;

    /// This describes the subject the certificate was issued to
    @JsonKey(required: true)
    @SubjectSerializer()
    final Subject subject;

    /// The allowed usgaes of the certificate
    ///
    /// For Signing certificates and ca root certificates: [Usage.VerifyCertificate].
    /// For all other: [Usage.Verify], [Usage.Encrypt]
    @JsonKey(required: true)
    final Set<Usage> usage;

    /// The commonName from the [subject]
    String get commonName => subject["commonName"]!;

    /// Checks if the certificate is valid based on [notBefore] and [notAfter] at the given point in time
    bool valid(DateTime now) =>  now.isBefore(notAfter) && now.isAfter(notBefore);

    /// Calls [valid] with the current time
    bool ValidNow() => valid(DateTime.now());

    /// Checks if the [operation] is allowed
    bool usageAllowed(Usage opertation) => usage.contains(opertation);

    bool operator ==(Object other) {
          
      if(! (other is ProtectedContent)) {
        return false;
      }

      return other.authority == authority &&
        jwkEqual(key, other.key) &&
        other.notAfter == notAfter &&
        other.notBefore == notBefore &&
        other.serialNumber == serialNumber &&
        other.subject == subject &&
        other.usage.containsAll(usage) &&
        usage.containsAll(other.usage);
    }

    // JSON
    factory ProtectedContent.fromJson(Map<String, dynamic> json) => _$ProtectedContentFromJson(json);
    Map<String, dynamic> toJson() => _$ProtectedContentToJson(this);

}

/// Describes the valid usages of the certificate
enum Usage {
  /// Allows the certificate to be used to verify signed content
  @JsonValue("VERIFY")
  Verify,

  /// Allows the certificate to be used to encrypt content
  @JsonValue("ENCRYPT")
  Encrypt,

  /// Allows the certificate to be used to verify child certificates
  @JsonValue("VERIFY_CERTIFICATE")
  VerifyCertificate
}
