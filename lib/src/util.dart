
import 'package:jose/jose.dart';

bool jwkEqual(JsonWebKey k1, JsonWebKey k2) {
    //TODO better
    return k1.keyId == k2.keyId;
}

void verifyArgument(bool expression, [ String? message ]) {
    if(! expression) {
        throw ArgumentError(message);
    }
}
