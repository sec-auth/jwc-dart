
import 'package:json_annotation/json_annotation.dart';

class SubjectSerializer implements JsonConverter<Subject, Map<String, dynamic>> {
  const SubjectSerializer();

  @override
  Subject fromJson(Map<String, dynamic> json) => Subject.fromJson(json);

  @override
  Map<String, dynamic> toJson(Subject object) => object.toJson();
}

typedef String CommonNameGenerator();

class NoCommonNameException implements Exception {

  const NoCommonNameException();

  @override
  String toString() => 'The CommonName is not given in the Request ond not generated while Approval';
}

/// Represents the subject the certificate is assigned to
class Subject {

  const Subject({ required this.commonName, this.additionalProperties = const{} });

  /// The common name of the subject, required
  final String commonName;

  /// Map with additional values
  final Map<String, String> additionalProperties;

  String? operator [](String key) => key == 'commonName' ? commonName : additionalProperties[key];

  factory Subject.fromJson(Map<String, dynamic> json){
    final p = <String, String>{};
    String? cn;

    for (final e in json.entries) {
      final v = e.value is String ? e.value : e.toString();

      if(e.key == 'commonName') {
        cn = v;
      } else {
        p[e.key] = v;
      }
    }

    return Subject(commonName: cn!, additionalProperties: p);

  }

  factory Subject.fromRequestSubject(Map<String, String> requestSubject, [ CommonNameGenerator? nameGenerator ] ) {
    nameGenerator ??= () { throw new NoCommonNameException(); };
    return Subject(
      commonName: requestSubject['commonName'] ?? nameGenerator(),
      additionalProperties: Map.fromEntries(
        requestSubject.entries.where((e) => e.key != 'commonName')
      )
    );
  }

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    json['commonName'] = commonName;
    json.addAll(additionalProperties);
    return json;
  }

  bool operator ==(Object other) {
    if (! (other is Subject)) {
      return false;
    }

    if(additionalProperties.length != other.additionalProperties.length) {
      return false;
    }

    if(commonName != other.commonName) {
      return false;
    }

    for(var e in additionalProperties.entries) {
      if(other[e.key] != e.value) {
        return false;
      }

      return true;
    }

    return true;
  }

}

