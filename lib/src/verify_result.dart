
import 'dart:async';

import './util.dart';

typedef FutureOr<VerifyResult> AsyncVerifier();

enum InvalidReason {

    CertExpired("The Certificate is expired"),
    ChainTooLong("The given certificate chain is too long"),
    ParentUnknown("There is a unknown certificate in the chain"),
    UntrustedRoot("The Root Certificate  is not trusted"),
    InvalidSignature("The given signature is invalid"),
    ParentCannotIssue("The parent certificate cannot issue certificates"),
    VerificationNotPossible('The Verification was not possible - unknown reason');

    const InvalidReason(this.description);

    final String description;

    @override
    String toString() => "${name}: ${description}";
}

/// The result of verifcating a [JsonWebCertificate] or [JsonWebCertificateRequest]
class VerifyResult {
    const VerifyResult.valid():this(true);

    const VerifyResult.invalid({ this.reason, this.exception }):
        valid = false;

    const VerifyResult(this.valid, [ this.reason, this.exception ]);
    
    final bool valid;
    final InvalidReason? reason;
    final Exception? exception;

    VerifyResult and(VerifyResult other) => VerifyResult(
        valid && other.valid,
        reason ?? other.reason,
        exception ?? other.exception,
    );

    @override
    String toString() {
        final parts = <String>[
            "The Certificate is",
            valid ? "valid" : "invalid",
            if (reason != null) "because ${reason.toString()}",
            if (exception != null) "because of exception ${exception.toString()}",
        ];

        return parts.join(" ");
    }

    @override
    bool operator ==(Object other) {
        if(other is VerifyResult) {
            return other.valid == valid && 
                reason == other.reason;
        }
        return false;
    }

    VerifyResult operator +(VerifyResult other) => and(other);

    factory VerifyResult.all(List<VerifyResult> results) {
        var current = results.first;
        for (var i = 1; i < results.length; i++) {
            current = current.and(results[i]);
        }
        return current;
    }

    /// Make a list of test
    ///
    /// The [tests] are executed until one fails. The failed [VerifyResult] is returned.
    /// If all [tetst] succeed, a valid [VerifyResult] is returned.
    static Future<VerifyResult> allAsync(List<AsyncVerifier> tests) async {
        verifyArgument(tests.isNotEmpty, 'There must be at least one test given');

        for (var test in tests) {
            final r = await test();
            if (! r.valid) {
                return r;
            }
        }

        return VerifyResult.valid();
    }
}
