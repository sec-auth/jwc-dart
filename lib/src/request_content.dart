
import 'package:json_annotation/json_annotation.dart';
import 'package:jose/jose.dart';

import './content.dart';
import './serialisers.dart';
import './util.dart';

part 'request_content.g.dart';

/// The content of a [JsonWebCertificateRequest]
///
/// Similar to [ProtectedContent]
@JsonSerializable(checked: true)
class RequestContent {

    const RequestContent({
        required this.key,
        required this.subject,
        required this.usage,
    });

    /// The Public key
    @JwkSerialiser()
    @JsonKey(required: true)
    final JsonWebKey key;

    /// The pretended subject
    @JsonKey(required: false, defaultValue: const {})
    final Map<String, String> subject;

    /// The usages the certificate shall be fore
    @JsonKey(required: false, defaultValue: const {})
    final Set<Usage> usage;

    @override
    bool operator ==(Object other) {
        if(other is RequestContent) {
            return jwkEqual(other.key, key) &&
                // subject == other.subject &&
                usage.length == other.usage.length &&
                usage.containsAll(other.usage);
        }
        else {
            return false;
        }
    }

    // JSON
    factory RequestContent.fromJson(Map<String, dynamic> json) => _$RequestContentFromJson(json);
    Map<String, dynamic> toJson() => _$RequestContentToJson(this);
}
