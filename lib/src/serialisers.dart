import 'package:json_annotation/json_annotation.dart';

import 'package:jose/jose.dart';

class JwsSerialiser implements JsonConverter<JsonWebSignature, Map<String, dynamic>> {

  const JwsSerialiser();

  @override
  JsonWebSignature fromJson(Map<String, dynamic> json) => JsonWebSignature.fromJson(json);

  @override
  Map<String, dynamic> toJson(JsonWebSignature jws) => jws.toJson();

}

class JwkSerialiser implements JsonConverter<JsonWebKey, Map<String, dynamic>> {

  const JwkSerialiser();

  @override
  JsonWebKey fromJson(Map<String, dynamic> json) => JsonWebKey.fromJson(json);

  @override
  Map<String, dynamic> toJson(JsonWebKey jwk) => jwk.toJson();

}
