
import 'dart:convert';

import './jwc.dart';

/// A simple implementation of [RootTester]
///
/// Checks JWCs against a set of SHA256 hashes
class TrustedRoots implements RootTester {

    TrustedRoots._(this._trusted);

    TrustedRoots():this._(Set<String>());

    TrustedRoots.of(Iterable<JsonWebCertificate> jwcs):this._(Set.from(jwcs.map((jwc) => _jwcHash(jwc))));

    final Set<String> _trusted;

    /// Adds a [jwc] to the list of trusted certificates
    void add(JsonWebCertificate jwc) {
        _trusted.add(_jwcHash(jwc));
    }

    static String _jwcHash(JsonWebCertificate jwc) => base64Url.encode(jwc.thumbprintSha256());

    @override
    bool isTrustedRoot(JsonWebCertificate jwc) {
        return _trusted.contains(_jwcHash(jwc));
    }
}
