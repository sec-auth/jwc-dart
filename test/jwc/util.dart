import 'package:crypto_keys/crypto_keys.dart';
import 'package:json_web_certificate/src/jwc.dart';
import 'package:json_web_certificate/src/content.dart';
import 'package:json_web_certificate/src/trusted_roots.dart';

import 'package:uuid/uuid.dart';
import 'package:jose/jose.dart';

CaRoot createChain({ int n = 3, TrustedRoots? roots }) {
    assert (n >= 1);
    var current = JsonWebCertificate.newCaRoot();

    roots?.add(current.certificate);

    for (var i = 1; i < n; i++) {
        current = JsonWebCertificate.createWithoutKey(
            parentPrivate: current.privateKey,
            parent: current.certificate,
            commonName: Uuid().v4(),
            usage: [Usage.VerifyCertificate]
        );
    }

    return current;
}

typedef ProtectedContent ContentFactory(JsonWebKey key);

CaRoot cerateCertWithContent(CaRoot ca, ContentFactory content) {
    final pair = KeyPair.generateRsa(bitStrength: 1024);
    final kid = Uuid().v4();
    final private = JsonWebKey.fromCryptoKeys(privateKey: pair.privateKey, keyId: kid);
    final public = JsonWebKey.fromCryptoKeys(publicKey: pair.publicKey, keyId: kid);

    final builder = JsonWebSignatureBuilder()
        ..jsonContent = content(public).toJson()
        ..addRecipient(ca.privateKey);

    final cert = JsonWebCertificate(
        signature: builder.build(),
        parent: ca.certificate,
    );

    return CaRoot(cert, private);

}
