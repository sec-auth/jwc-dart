import 'package:json_web_certificate/src/jwc.dart';
import 'package:json_web_certificate/src/trusted_roots.dart';


import 'package:test/test.dart';

import './util.dart';


void main() {
    group('JWC Serialisation', (){
        test('JWC Root Serialization', () async {
            final ca = JsonWebCertificate.newCaRoot();

            final json = ca.certificate.toJson();

            final cert = JsonWebCertificate.fromJson(json);

            final verification = await cert.verifyWith(cert);

            expect(verification.valid, true, reason: 'The deserialized JWC must be valid');
            expect(cert.content == ca.certificate.content, true, reason: 'The Content of the original and deserialized JWC must eb equal.');
        });

        test('JWC Chain Serialization', () async {

            final roots = TrustedRoots();
            final bundle = createChain(roots: roots);

            final json = bundle.certificate.toJson();

            final cert = JsonWebCertificate.fromJson(json);

            final verification = await cert.verify(roots);

            expect(verification.valid, true, reason: 'The deserialized JWC must be valid');

        });
    });
}
