
import 'package:json_web_certificate/src/content.dart';
import 'package:json_web_certificate/src/trusted_roots.dart';
import 'package:json_web_certificate/src/verify_result.dart';
import 'package:json_web_certificate/src/subject.dart';

import 'package:uuid/uuid.dart';
import 'package:test/test.dart';

import './util.dart';

void main () {
    group('test JWC verification', (){
        test('JWC Chain positive', () async {
            final roots = TrustedRoots();
            final pair = createChain(roots: roots);

            final result = await pair.certificate.verify(roots);

            expect(result.valid, true);
            expect(result.reason, null);
            expect(result.exception, null);
        });

        test('JWC Verification Fail: untrusted root', () async {
            final roots = TrustedRoots();
            final pair = createChain();

            final result = await pair.certificate.verify(roots);

            expect(result.valid, false);
            expect(result.reason, InvalidReason.UntrustedRoot);
        });

        test('JWC Verification fail: expired', () async {
            final roots = TrustedRoots();
            final pair = createChain(roots: roots);
            final badPair = cerateCertWithContent(
                pair,
                (key) => ProtectedContent(
                    serialNumber: Uuid().v4(),
                    authority: pair.certificate.content.commonName,
                    notBefore: DateTime.now().add(Duration(days: 3)),
                    notAfter: DateTime.now().add(Duration(days: 4)),
                    key: key,
                    subject: Subject(commonName: Uuid().v4()),
                    usage: {}
                )
            );

            final result = await badPair.certificate.verify(roots);

            expect(result.valid, false);
            expect(result.reason, InvalidReason.CertExpired);
        });

        test('JWC Verification Fail: chain too long', () async {
            final roots = TrustedRoots();
            final pair = createChain(roots: roots, n: 10);

            final result = await pair.certificate.verify(roots);

            expect(result.valid, false);
            expect(result.reason, InvalidReason.ChainTooLong);
        });

        test('JWC Verification fail: parent cannot issue', () async {
            final roots = TrustedRoots();
            var pair = createChain(roots: roots, n: 2);

            for(var i = 0; i < 2; i++) {

                pair = cerateCertWithContent(
                    pair,
                    (key) => ProtectedContent.create(
                        authority: pair.certificate.content.commonName,
                        key: key,
                        subject: Subject(commonName: Uuid().v4()),
                        usage: { Usage.Encrypt, Usage.Verify }
                    )
                );

            }

            final result = await pair.certificate.verify(roots);

            expect(result.valid, false);
            expect(result.reason, InvalidReason.ParentCannotIssue);
        });
    });
}
