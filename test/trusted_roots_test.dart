
import 'package:test/test.dart';
import 'package:json_web_certificate/src/trusted_roots.dart';
import 'package:json_web_certificate/src/jwc.dart';

void main() {
    group('Test TrustedRoots class', (){
        test('Test positive', (){
            final roots = TrustedRoots();

            for(var i = 0; i < 5; i++) {
                final ca = JsonWebCertificate.newCaRoot();
                roots.add(ca.certificate);
            }

            final ca = JsonWebCertificate.newCaRoot();
            roots.add(ca.certificate);

            expect(roots.isTrustedRoot(ca.certificate), true);
        });

        test('Test negative', (){
            final roots = TrustedRoots();

            for(var i = 0; i < 5; i++) {
                final ca = JsonWebCertificate.newCaRoot();
                roots.add(ca.certificate);
            }

            final notCa = JsonWebCertificate.newCaRoot();

            expect(roots.isTrustedRoot(notCa.certificate), false);
        });

        test('Test positive & negative large set', (){
            final roots = TrustedRoots();

            for(var i = 0; i < 20; i++) {
                final ca = JsonWebCertificate.newCaRoot();
                roots.add(ca.certificate);
            }

            final notCa = JsonWebCertificate.newCaRoot();

            final ca = JsonWebCertificate.newCaRoot();
            roots.add(ca.certificate);

            expect(roots.isTrustedRoot(ca.certificate), true);
            expect(roots.isTrustedRoot(notCa.certificate), false);
        });
    });
}

