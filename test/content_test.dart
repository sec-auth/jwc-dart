import 'package:jose/jose.dart';
import 'package:json_web_certificate/json_web_certificate.dart';
import 'package:test/test.dart';

void main() {
  group('Protected Content', () {
    test('Serialization', (){
      final c = ProtectedContent.create(
        authority: "a", 
        key: JsonWebKey.generate('ES512'), 
        subject: Subject(commonName: 'de.de.de'),
        usage: Set.of([Usage.VerifyCertificate])
      );

      final json = c.toJson();

      final c1 = ProtectedContent.fromJson(json);

      expect(c == c1, true, reason: 'The parsed and the original Content must be equal');
    });
  });
}