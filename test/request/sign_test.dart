import 'package:test/test.dart';

import 'package:json_web_certificate/src/subject.dart';
import 'package:json_web_certificate/src/jwc.dart';
import 'package:json_web_certificate/src/request.dart';
import 'package:json_web_certificate/src/trusted_roots.dart';

void main() {
    group('Sign a JWC Request', (){
        test('Creation', () async {
            final g = JsonWebCertificateRequest.createWithoutKey(subject: {"commonName": "test"});
            final r = await g.request.validate();

            expect(r.valid, true, reason: 'The generated Request must be valid');
        });

        test('approve', () async {
            final req = JsonWebCertificateRequest.createWithoutKey(subject: {"commonName": "test"}).request;

            final ca = JsonWebCertificate.newCaRoot();
            final roots = TrustedRoots()
                ..add(ca.certificate);

            var r = await req.validate();
            expect(r.valid, true, reason: 'The generated Request must be valid');

            final child = req.approve(ca.certificate, ca.privateKey);

            r = await child.verify(roots);
            expect(r.valid, true);
        });

        test('Approve with generated commonName', () async {
            final req = JsonWebCertificateRequest.createWithoutKey().request;
            final ca = JsonWebCertificate.newCaRoot();

            var r = await req.validate();
            expect(r.valid, true, reason: 'The generated Request must be valid');

            final child = req.approve(ca.certificate, ca.privateKey, nameGenerator: () => 'generated_cn');

            expect(child.content.subject.commonName, 'generated_cn', reason: 'The common name must match the given one');
            expect(child.content.subject.additionalProperties, isEmpty);

        });

        test('Approve with SubjectOverrider', () async {
            final req = JsonWebCertificateRequest.createWithoutKey(subject: {
                'commonName': 'cn_set_in_request',
                'isRoot': 'true',
                'priv': 'true'
            }).request;
            final ca = JsonWebCertificate.newCaRoot();

            var r = await req.validate();
            expect(r.valid, true, reason: 'The generated Request must be valid');

            final child = req.approve(ca.certificate, ca.privateKey, subjectOverrider: (rs) => Subject(
                commonName: 'cn_set_by_ca',
                additionalProperties: {
                    'priv': 'false'
                }
            ) );

            expect(child.content.subject.commonName, 'cn_set_by_ca');
            expect(child.content.subject.additionalProperties, allOf([
                containsPair('priv', 'false'),
                hasLength(1)
            ]));

        });

        test('Fail Approve without commonName', () async {
            final req = JsonWebCertificateRequest.createWithoutKey().request;
            final ca = JsonWebCertificate.newCaRoot();

            var r = await req.validate();
            expect(r.valid, true, reason: 'The generated Request must be valid');

            expect(() => req.approve(ca.certificate, ca.privateKey), throwsA(isA<NoCommonNameException>()));

        });
    });
}