import 'package:test/test.dart';

import 'package:json_web_certificate/json_web_certificate.dart';

void main() {
  group('Create, sign a renew request', () {
    test('', () async {
      final ca = JsonWebCertificate.newCaRoot();
      final old = JsonWebCertificate.createWithoutKey(parentPrivate: ca.privateKey, parent: ca.certificate, commonName: 'some cn');
      final req = JsonWebCertificateRequest.createWithoutKey(oldPrivate: old.privateKey);

      expect(req.request.isRenewRequest, true);

      final res = await req.request.validate(old.certificate);
      expect(res.valid, true);
    });
  });
}