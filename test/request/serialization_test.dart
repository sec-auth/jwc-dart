import 'package:test/test.dart';

import 'package:json_web_certificate/src/request.dart';

import 'dart:convert';

void main() {
    group('Serialize a JWC Request', () {
        test('Serialize & deserialize', () async {
            final req1 = JsonWebCertificateRequest.createWithoutKey(subject: {"commonName": "test"}).request;

            var json = req1.toJson();
            json = jsonDecode(jsonEncode(json)) as Map<String, dynamic>;

            final req2 = JsonWebCertificateRequest.fromJson(json);

            final r = await req2.validate();
            expect(r.valid, true, reason: 'Deserialized request must be valid');

            expect(req1.content == req2.content, true, reason: 'The Contents before and after serialization must be equal');
        });
    });
}
