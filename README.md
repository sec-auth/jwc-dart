
This Library adds JSON based Certificates to the Javascript Object Signing and Encryption (JOSE) standard. The JSON Web Certtificates (JWC) and JWC Requests are based on the JSON Web Signature (JWS). This library is based on the [JOSE library](https://pub.dev/packages/jose).

# Supported JSON Web Algorithms

This library supports the following JSON Web Algorithms (JWA):

| JWA    | Description                     |
| ------ | ------------------------------- |
| RS256  | RSASSA-PKCS1-v1_5 using SHA-256 |
| RS384  | RSASSA-PKCS1-v1_5 using SHA-384 |
| RS512  | RSASSA-PKCS1-v1_5 using SHA-512 |
| ES256  | ECDSA using P-256 and SHA-256   |
| ES256K | ECDSA using P-256K and SHA-256  |
| ES384  | ECDSA using P-384 and SHA-384   |
| ES512  | ECDSA using P-521 and SHA-512   |

# Usage

## Parse and verify a JWC

```dart

void main() async {
    final jwc = JsonWebCertificate.fromJson(jsonDecode('{ ... }'));

    final trustedRoots = TrustedRoots()
        ...add(trustedRootJwc);

    final result = await jwc.verify(trustedRoots);

    print(result.valid ? 'JWC is valid' : result.toString());
}

```

## Sign a JWC Request

```dart
void main() async {
    // A Parent Certificate an its private key is needed
    final ca = JsonWebCertificate.newCaRoot();
    
    final request = JsonWebCertificateRequest.fromJson(jsonDecode('{ ... }'));

    final result = await request.validate();

    if(! result.valid) {
        print("The Request is invalid: ${result}");
        return;
    }

    final newCertificate = request.approve(ca.certificate, ca.privateKey);

    // Do something with the new certificate

}
```

## Create a JWC Request

```dart
void main () async {

    final myId = '......';
    
    // Generate the Request and the key at once
    final generated = JsonWebCertificateRequest.createWithoutKey(
        Subject(commonName: myId),      // The Subject
        [ Usage.Encrypt, Usage.Verify ] // The requested Usage
    );

    // Serialize to JSON
    final json = jsonEncode(generated.request.toJson());

    // Send the Request to a CA to sign it
}
```
